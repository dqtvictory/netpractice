#pragma once

#include <iostream>
#include <sstream>
#include <cstdio>

using namespace std;

class IPv4
{
	static unsigned int	ipValueConv(unsigned int ipv4[4])
	{
		unsigned int	value = 0;

		for (int i = 0; i < 4; i++)
			value |= ipv4[i] << (8 * (3 - i));
		return (value);
	}

	static unsigned int	ipValueFromCidr(int cidr)
	{
		unsigned int	value = 1 << 31;
		for (int i = 1; i < cidr; i++)
			value |= value >> 1;
		return (value);
	}

	unsigned int const	_value;

	unsigned int	getOctet(unsigned int pos) const
	{
		if (pos > 3)
			return (256);

		int				startBitPos = 8 * (4 - pos) - 1;
		unsigned int	num = 1 << startBitPos;
		for (int i = 0; i < 8; i++)
			num |= (num >> 1);
		return ((num & _value) >> 8 * (3 - pos));
	}

public:
	static IPv4	cidrToSubnetMask(int cidr)	{ return (IPv4(ipValueFromCidr(cidr))); }
	int			toCidr(void) const
	{
		int	count = 0;
		for (unsigned int mask = 1 << 31; mask & _value; mask >>= 1)
			count++;
		return (count);
	}

	IPv4(void) : _value(0) {}
	IPv4(unsigned int value) : _value(value) {}
	IPv4(unsigned int ipv4[4]) : _value(ipValueConv(ipv4)) {}

	bool	operator==(IPv4 const &ip) const	{ return (_value == ip._value); }
	bool	operator!=(IPv4 const &ip) const	{ return (_value != ip._value); }

	IPv4	getNetworkIP(IPv4 const &subnetMask) const		{ return (IPv4(subnetMask._value & _value)); }
	IPv4	getNetworkIP(int cidr) const 					{ return (getNetworkIP(IPv4(ipValueFromCidr(cidr)))); }

	IPv4	getBroadcastIP(IPv4 const &subnetMask) const	{ return (IPv4(_value | ~subnetMask._value)); }
	IPv4	getBroadcastIP(int cidr) const 					{ return (getBroadcastIP(IPv4(ipValueFromCidr(cidr)))); }

	IPv4	getNetRangeStart(IPv4 const &subnetMask) const	{ return (IPv4(getNetworkIP(subnetMask)._value + 1)); }
	IPv4	getNetRangeStart(int cidr) const 				{ return (getNetRangeStart(IPv4(ipValueFromCidr(cidr)))); }

	IPv4	getNetRangeEnd(IPv4 const &subnetMask) const	{ return (IPv4(getBroadcastIP(subnetMask)._value - 1)); }
	IPv4	getNetRangeEnd(int cidr) const 					{ return (getNetRangeEnd(IPv4(ipValueFromCidr(cidr)))); }

	unsigned int	getNetHostCount(IPv4 const &subnetMask) const	{ return (~subnetMask._value - 1); }
	unsigned int	getNetHostCount(int cidr) const					{ return ((1 << (32 - cidr)) - 2); }

	bool	isSubnetMask(void) const
	{
		if (_value == 0)
			return (false);

		unsigned int	bitMask = 1 << 31;
		int				currentBit	= 1;
		for (int i = 0; i < 32; i++)
		{
			if (currentBit == 0 && (bitMask & _value))
				return (false);
			else if ((bitMask & _value) == 0)
				currentBit = 0;
			bitMask >>= 1;
		}
		return (true);
	}

	static bool	isValidIPv4(char const *str)
	{
		unsigned int	o1, o2, o3, o4;
		int	scan = sscanf(str, "%u.%u.%u.%u", &o1, &o2, &o3, &o4);
		return (scan == 4 && o1 <= 255 && o2 <= 255 && o3 <= 255 && o4 <= 255);
	}

	friend ostream	&operator<<(ostream &os, IPv4 const &ipv4);
};

ostream	&operator<<(ostream &os, IPv4 const &ip)
{
	for (int i = 0; i < 4; i++)
	{
		os << ip.getOctet(i);
		if (i != 3)
			os << '.';
	}
	return (os);
}
