#include <vector>
#include <cstdlib>
#include "IPv4.h"

int	errorMsg(string msg)
{
	cout << msg << '\n';
	return (1);
}

string	decToBin(int num)
{
	stringstream	ss;
	for (int mask = 1 << 7; mask > 0; mask >>= 1)
	{
		if (num & mask)
			ss << '1';
		else
			ss << '0';
	}
	return (ss.str());
}

int	binToDec(string bin)
{
	int	dec = 0, pow2 = 1;
	for (string::reverse_iterator it = bin.rbegin(); it != bin.rend(); it++)
	{
		if (*it == '1')
			dec += pow2;
		pow2 <<= 1;
	}
	return (dec);
}

void	displayInfo(IPv4 const &ip, IPv4 const &subnetMask)
{
	cout	<< "Host IP:      " << ip << '\n'
			<< "Subnet Mask:  " << subnetMask << '\n'
			<< "CIDR format:  " << ip << '/' << subnetMask.toCidr() << '\n' 
			<< "Network IP:   " << ip.getNetworkIP(subnetMask) << '\n'
			<< "Broadcast IP: " << ip.getBroadcastIP(subnetMask) << '\n'
			<< "Range from:   " << ip.getNetRangeStart(subnetMask) << '\n'
			<< "Range to:     " << ip.getNetRangeEnd(subnetMask) << '\n'
			<< "Host count:   " << ip.getNetHostCount(subnetMask) << '\n';
}

int	b2d(int size, char **binStr)
{
	string	bin;

	for (int i = 0; i < size; i++)
	{
		bin = binStr[i];
		if (bin.size() > 8)
			return (errorMsg("Found a binary having more than 8 bits"));
		for (string::iterator it = bin.begin(); it != bin.end(); it++)
			if (*it != '0' && *it != '1')
				return errorMsg("Found an invalid binary (only 0 and 1 are allowed)");
	}

	for (int i = 0; i < size; i++)
		cout << binToDec(binStr[i]) << ' ';
	cout << endl;
	return (0);
}

int	d2b(int size, char **decStr)
{
	string		dec;
	int			num;
	vector<int>	decArray;

	for (int i = 0; i < size; i++)
	{
		dec = decStr[i];
		if (dec.size() > 3)
			return (errorMsg("Found a decimal too large for a 8-bit unsigned integer"));
		for (string::iterator it = dec.begin(); it != dec.end(); it++)
			if (*it < '0' || *it > '9')
				return (errorMsg("Found a non-numerical character in a decimal number"));
		num = atoi(decStr[i]);
		if (num > 255)
			return (errorMsg("Found a decimal too large for a 8-bit unsigned integer"));
		decArray.push_back(num);
	}

	for (vector<int>::iterator it = decArray.begin(); it != decArray.end(); it++)
		cout << decToBin(*it) << ' ';
	cout << endl;
	return (0);
}

int	ipv4cidr(char const *arg)
{
	unsigned int	o1, o2, o3, o4, cidr;
	int	scan = sscanf(arg, "%u.%u.%u.%u/%u", &o1, &o2, &o3, &o4, &cidr);
	if (scan != 5 || o1 > 255 || o2 > 255 || o3 > 255 || o4 > 255 || cidr > 31)
		return (errorMsg("Wrong format for IP address using CIDR network mask"));
	
	IPv4	ip((unsigned int[]){o1, o2, o3, o4});
	IPv4	subnetMask = IPv4::cidrToSubnetMask(cidr);
	displayInfo(ip, subnetMask);
	return (0);
}

int ipv4subnet(char const *ipStr, char const *subnetStr)
{
	if (!IPv4::isValidIPv4(ipStr) || !IPv4::isValidIPv4(subnetStr))
		return (errorMsg("Wrong format for IP address or subnet mask"));
	
	unsigned int	o1, o2, o3, o4;
	sscanf(ipStr, "%u.%u.%u.%u", &o1, &o2, &o3, &o4);
	IPv4	ip((unsigned int[]){o1, o2, o3, o4});

	sscanf(subnetStr, "%u.%u.%u.%u", &o1, &o2, &o3, &o4);
	IPv4	subnetMask((unsigned int[]){o1, o2, o3, o4});
	if (!subnetMask.isSubnetMask())
		return (errorMsg("Invalid subnet mask"));

	displayInfo(ip, subnetMask);
	return (0);
}

int main(int ac, char **av)
{
	if (ac == 1)
	{
		cout	<< "NetPractice tool must take the following arguments:\n"
				<< "b2d <bin1> <bin2> ... <binN>: convert N binary numbers to decimal as long as they are valid 8-bit binaries\n"
				<< "d2b <dec1> <dec2> ... <devN>: convert N positive numbers to 8-bit binary. Numbers must fit into an 8-bit unsigned integer\n"
				<< "<ipv4>/<cidr>: display network info for an IP address using CIDR network mask format (for example: 192.168.1.10/24)\n"
				<< "<ipv4> <subnet mask>: display network infor for an IP address using its subnet mask (for example: 192.168.1.10 255.255.255.0)\n";
		return (0);
	}

	string	av1(av[1]);
	if (av1 == "b2d")
		return (b2d(ac - 2, av + 2));
	else if (av1 == "d2b")
		return (d2b(ac - 2, av + 2));
	else if (av1.find('/') != string::npos && ac == 2)
		return (ipv4cidr(av[1]));
	else if (av1.find('/') == string::npos && ac == 3)
		return (ipv4subnet(av[1], av[2]));
	else
		return (errorMsg("Wrong arguments"));
}
